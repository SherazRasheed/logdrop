//
//  CityLocation.swift
//  LOGDROP
//
//  Created by Shery on 17/08/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

//<span; strlen="font-size:16px;"><span; style="font-family:arial,helvetica,sans-serif;">;
import Foundation
import MapKit

class CityLocation: NSObject, MKAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    
    
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}
//</span></span>
