//
//  GMLoginViewController.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit
import Firebase
import PINCache
import MapKit
import OneSignal

class GMMapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    // MARK: Outlets
    @IBOutlet weak var loginView: GMMapView!
    //   @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var pinImageVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapCenterPinImage: UIImageView!
    
    var coordinates: [[Double]]!
    var names:[String]!
    var addresses:[String]!
    var phones:[String]!
    
    var fetchedReferals:[String:String]?
    
    var referalData = [UserClass]()
    
    @IBOutlet weak var mapView: MKMapView!
    var userPinView: MKAnnotationView!
    
    
    // MARK: Instance Variables
    var handler: GMMapHandler!
    var searchedTypes = ["bakery", "bar", "cafe"]
    let locationManager = CLLocationManager()
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        
        
        coordinates = [[48.85672,2.35501],[48.85196,2.33944],[48.85376,2.33953]]// Latitude,Longitude
                names = ["Coffee Shop · Rue de Rivoli","Cafe · Boulevard Saint-Germain","Coffee Shop · Rue Saint-André des Arts"]
                addresses = ["46 Rue de Rivoli, 75004 Paris, France","91 Boulevard Saint-Germain, 75006 Paris, France","62 Rue Saint-André des Arts, 75006 Paris, France"]
                phones = ["+33144789478","+33146345268","+33146340672"]
                self.mapView.delegate = self
                // 2
                for i in 0...2
                {
                    let coordinate = coordinates[i]
                    let point = StarbucksAnnotation(coordinate: CLLocationCoordinate2D(latitude: coordinate[0] , longitude: coordinate[1] ))
                    point.name = names[i]
                    point.address = addresses[i]
                    point.phone = phones[i]
                    point.type = searchedTypes[i]
                    self.mapView.addAnnotation(point)
                }
        
        //
        //        coordinates = [[48.85672,2.35501],[48.85196,2.33944],[48.85376,2.33953]]// Latitude,Longitude
        //        names = ["Coffee Shop · Rue de Rivoli","Cafe · Boulevard Saint-Germain","Coffee Shop · Rue Saint-André des Arts"]
        //        addresses = ["46 Rue de Rivoli, 75004 Paris, France","91 Boulevard Saint-Germain, 75006 Paris, France","62 Rue Saint-André des Arts, 75006 Paris, France"]
        //        phones = ["+33144789478","+33146345268","+33146340672"]
        //        self.mapView.delegate = self
        //        // 2
        //        for i in 0...2
        //        {
        //            let coordinate = coordinates[i]
        //            let point = StarbucksAnnotation(coordinate: CLLocationCoordinate2D(latitude: coordinate[0] , longitude: coordinate[1] ))
        //            point.name = names[i]
        //            point.address = addresses[i]
        //            point.phone = phones[i]
        //            point.type = "Log"
        //            self.mapView.addAnnotation(point)
        //        }
        //
        // 3
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 37.785834, longitude: -122.406417), span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        self.mapView.setRegion(region, animated: true)
        
        //Configurations
        self.configureNavigationBar()
        self.configureHandler()
        self.configureCallBacks()
        self.updateMapsLocations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        self.configureView()
    }
    
    // MARK: Private Methods
    //MARK: Gmail Login
    
    // MARK: Navigation Bar
    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: Handler
    private func configureHandler() -> Void{
        self.handler = GMMapHandler(viewController: self)
    }
    
    // MARK: View
    private func configureView() {
    }
    
    // MARK: Callbacks
    func configureCallBacks(){
        self.loginView.didPressLoginButtonCallback = {
        }
        
        self.loginView.didPressSignUpButtonCallback = {
        }
        
        self.loginView.didPressforgotPasswordButtonCallback = {
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        var locationArray = locations as NSArray
        var locationObj = locationArray.lastObject as! CLLocation
        var coord = locationObj.coordinate
        print(coord.latitude)
        print(coord.longitude)
        
    }
    
    func updateMapsLocations() {
        
        
        
        Database.database().reference().child("users").child("0BtxOD1PFaQHcHZ7auA9yRzmNON2").child("onesignal").observeSingleEvent(of: .value, with: {snapshot in
            if snapshot.exists() {
                
                for (playerId, _) in snapshot.value as! Dictionary<String, String> {
                    
                    OneSignal.postNotification(["headings": ["en": "ewdewee"], "contents": ["en": "Goottttt"], "include_player_ids": [playerId]])
                }
            }
        })
        
        let userReference = Database.database().reference()
        userReference.child("TreeUser").observeSingleEvent(of: .value, with: { (snapshot) in
            if let result = snapshot.children.allObjects as? [DataSnapshot] {
                for child in result {
                    //do your logic and validation here
                    //     child.value["name"] as! String
                    self.fetchedReferals = child.value as? [String : String]
                    let userData = UserClass()
                    userData.fetchRefferal(snap: child)
                    self.referalData.append(userData)
                    let coordinateLatitude = self.fetchedReferals?["latitude"]
                    let coordinateLongitude = self.fetchedReferals?["longitude"]
                    
                    let point = StarbucksAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(coordinateLatitude!)! , longitude: Double(coordinateLongitude!)! ))
                    point.name = self.fetchedReferals?["name"]
                    point.address = self.fetchedReferals?["address"]
                    point.phone = self.fetchedReferals?["phone"]
                    point.type = (self.fetchedReferals?["type"])!
                    self.mapView.addAnnotation(point)
                    
                }
            } else {
                print("no results")
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        userReference.child("HouseUser").observeSingleEvent(of: .value, with: { (snapshot) in
            if let result = snapshot.children.allObjects as? [DataSnapshot] {
                for child in result {
                    //do your logic and validation here
                    //     child.value["name"] as! String
                    self.fetchedReferals = child.value as? [String : String]
                    let userData = UserClass()
                    userData.fetchRefferal(snap: child)
                    self.referalData.append(userData)
                    let coordinateLatitude = self.fetchedReferals?["latitude"]
                    let coordinateLongitude = self.fetchedReferals?["longitude"]
                    
                    let point = StarbucksAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(coordinateLatitude!)! , longitude: Double(coordinateLongitude!)! ))
                    point.name = self.fetchedReferals?["name"]
                    point.address = self.fetchedReferals?["address"]
                    point.phone = self.fetchedReferals?["phone"]
                    point.type = (self.fetchedReferals?["type"])!
                    self.mapView.addAnnotation(point)
                    
                }
            } else {
                print("no results")
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
//        
//                if Auth.auth().currentUser != nil {
//        
//                    let userReference = Database.database().reference()
//        
//                    userReference.child("User").observe(.value, with: { (snapshot) in
//        
//                        //Convert the info of the data into a string variable
//        
//                        let userData = UserClass()
//                        userData.fetchRefferal(snap: snapshot)
//                        self.referalData.append(userData)
//                        self.fetchedReferals = snapshot.value as! [String : String]
//        
//                        let coordinateLatitude = self.fetchedReferals?["latitude"]
//                        let coordinateLongitude = self.fetchedReferals?["longitude"]
//        
//                        let point = StarbucksAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(coordinateLatitude!)! , longitude: Double(coordinateLongitude!)! ))
//                        point.name = self.fetchedReferals?["name"]
//                        point.address = self.fetchedReferals?["address"]
//                        point.phone = self.fetchedReferals?["phone"]
//                        point.type = self.fetchedReferals?["type"]
//                        self.mapView.addAnnotation(point)
//                    })
//                }
    }
    
    //MARK: MKMapViewDelegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        
        let myAnnotation1 = (annotation as! StarbucksAnnotation)
        annotationView?.image = UIImage(named: myAnnotation1.type)
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let starbucksAnnotation = view.annotation as! StarbucksAnnotation
        let views = Bundle.main.loadNibNamed("CustomCalloutView", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCalloutView
        calloutView.starbucksName.text = starbucksAnnotation.name
        calloutView.starbucksAddress.text = starbucksAnnotation.address
        calloutView.starbucksPhone.text = starbucksAnnotation.phone
        
        //
        let button = UIButton(frame: calloutView.starbucksPhone.frame)
        button.addTarget(self, action: #selector(self.callPhoneNumber(sender:)), for: .touchUpInside)
        calloutView.addSubview(button)
        //        calloutView.starbucksImage.image = starbucksAnnotation.image
        // 3
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        view.addSubview(calloutView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
    }
    
    func callPhoneNumber(sender: UIButton)
    {
        let v = sender.superview as! CustomCalloutView
        if let url = URL(string: "telprompt://\(v.starbucksPhone.text!)"), UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.openURL(url)
        }
    }
}
