//
//  GMLoginHandler.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation

class GMMapHandler: NSObject {
    // MARK: Instance Variables
    var viewController: GMMapViewController!
    var view: GMMapView!
    
    // MARK: Init
    required init(viewController: GMMapViewController!) {
        self.viewController = viewController
        self.view = self.viewController.loginView
    }
    
    //MARK: Callbacks

}

