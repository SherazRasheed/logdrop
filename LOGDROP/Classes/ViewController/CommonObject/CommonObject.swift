//
//  CommonObject.swift
//  RenaissanceAssociates
//
//  Created by Shery on 07/08/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation
import Firebase


class UserClass {
    
    var name = String()
    var email = String()
    var secondName = String()
    var phone = String()
    var state = String()
    var zip = String()
    var location = String()
    var latitide = String()
    var requirements = String()
    var logitude = String()
    var type = String()
    var id = String()
    var userReference: DatabaseReference!
    
    func fetchRefferal(snap: DataSnapshot) {
        
        let userDict = snap.value as! [String: Any]
        
        self.id = snap.key
        
        self.name = userDict["name"] as! String
        self.phone = userDict["phone"] as! String
        self.location = userDict["address"] as! String
        self.latitide = userDict["latitude"] as! String
        self.logitude = userDict["longitude"] as! String
        self.type = userDict["type"] as! String
    }
    
    func fetchUser(snap: DataSnapshot) {
        
        let userDict = snap.value as! [String: Any]
        
        self.id = snap.key
        
        self.name = userDict["name"] as! String
        self.phone = userDict["phone"] as! String
        self.location = userDict["location"] as! String
        self.requirements = userDict["Requirements"] as! String
    }
    
    func addNewUser(name:String,email:String,phoneNumber:String,location:String,bloodGroup:String,availble:String){
        
        userReference = Database.database().reference()
        
        let key = userReference.child("Users").childByAutoId().key
        
        let user: NSDictionary = ["name": name,
                                      "phone": phone,
                                      "address": location,
                                      "comments": requirements
        ]
        let childUpdates = ["/posts/\(key)": user]
        userReference.updateChildValues(childUpdates)
    }
}
