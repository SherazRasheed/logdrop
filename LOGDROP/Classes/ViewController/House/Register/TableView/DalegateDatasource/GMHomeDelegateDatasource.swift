//
//  GMContestDelegateDatasource.swift
//  GlobalGame
//
//  Created by Shery on 31/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit

class GMHomeDelegateDatasource: UITableView, UITableViewDelegate, UITableViewDataSource {
    
    public var didPressHomeButton: ((_ Ingredients:String,_ title:String) -> Void)?
    public var didPressLikeButton: ((_ index:String) -> Void)?
    public var isTableLoaded: (()-> Void)?
    public var didUpdateTableCell: ((_ index:Int)-> Void)?
    var collectionViewData = [UserClass]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate=self
        self.dataSource=self
        
        self.register(UINib(nibName: ApplicationInterfaceFilenames.kHomeTableViewCell, bundle: nil), forCellReuseIdentifier: ApplicationInterfaceFilenames.kHomeTableViewCell)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collectionViewData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ApplicationInterfaceFilenames.kHomeTableViewCell, for: indexPath)  as!
        GMHomeTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if didUpdateTableCell != nil {
            didUpdateTableCell!(indexPath.row)
        }
    }
}
