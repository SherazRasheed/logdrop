//
//  GMContestTableViewCell.swift
//  GlobalGame
//
//  Created by Shery on 31/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit

class GMHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var gameTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
