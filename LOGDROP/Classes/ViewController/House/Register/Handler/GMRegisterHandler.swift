//
//  GMRegisterHandler.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation

class GMRegisterHandler: NSObject {
    // MARK: Instance Variables
    var viewController: GMRegisterViewController!
    var view: GMRegisterView!
    
    // MARK: Init
    required init(viewController: GMRegisterViewController!) {
        self.viewController = viewController
        self.view = self.viewController.registerView
    }
    
    //MARK: Callbacks
    
}

