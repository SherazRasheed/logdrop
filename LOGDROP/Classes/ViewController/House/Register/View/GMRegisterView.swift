//
//  GMView.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation
import UIKit

class GMRegisterView: UIView {
    // MARK: Outlets
    
    @IBOutlet weak var hometable: GMHomeDelegateDatasource!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var confromPasswordTextfield: UITextField!
    @IBOutlet weak var requirementTextfield: UITextField!
    @IBOutlet weak var locationTextfield: UITextField!

    
    
    
    @IBOutlet weak var signUpText: UILabel!
    @IBOutlet weak var numberOfReferals: UILabel!
    @IBOutlet weak var noReferalLabel: UILabel!

    @IBOutlet weak var affiliateButton: UIButton!
    
    @IBOutlet weak var refferalButton: UIButton!
    
    override func awakeFromNib() {
        
        signUpText.font = UIFont(name: "BahamasPSMT", size: 20.0)
    }
    
    public var didPressLoginButtonCallback : (() -> Void)?
    public var didPressSignUpButtonCallback : (() -> Void)?

    // MARK: Action Methods
    @IBAction func loginButtonPressed(_ sender: Any) {
        self.handleLoginButtonTapEvent()
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        self.handleSignUpButtonTapEvent()
    }
    
    //MARK: Events
    private func handleLoginButtonTapEvent() {
        if didPressLoginButtonCallback != nil {
            didPressLoginButtonCallback!()
        }
    }
    
    private func handleSignUpButtonTapEvent() {
        if didPressSignUpButtonCallback != nil {
            didPressSignUpButtonCallback!()
        }
    }
}

