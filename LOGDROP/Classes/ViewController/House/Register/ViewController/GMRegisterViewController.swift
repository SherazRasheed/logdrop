//
//  GMRegisterViewController.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit
import Firebase
import PINCache
import KYDrawerController
import NVActivityIndicatorView
import FirebaseMessaging
import OneSignal
import CoreLocation
import Stripe

class GMRegisterViewController: UIViewController,CLLocationManagerDelegate {
    // MARK: Outlets
    @IBOutlet weak var registerView: GMRegisterView!
    var locationManager:CLLocationManager!
    
    // MARK: Instance Variables
    var handler: GMRegisterHandler!
    var referalIds = [String]()
    var fetchedReferals:[String:Any]?
    var referalData = [UserClass]()
    var userLocationLatitude:String?
    var userLocationLongitude: String?
    var treeUserIds = [String]()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations[0]
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        
        manager.stopUpdatingLocation()
        
        userLocationLatitude = String(userLocation.coordinate.latitude)
        userLocationLongitude = String(userLocation.coordinate.longitude)
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        fetchCountryAndCity(location: userLocation) { country, city in
            print("country:", country)
            print("city:", city)
            self.registerView.locationTextfield.text = city + " " + country
        }
        
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Configurations
        //        self.configureIndicator()
        self.configureNavigationBar()
        self.configureHandler()
        self.configureCallBacks()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        self.configureView()
    }
    
    // MARK: Private Methods
    
    // MARK: Navigation Bar
    
    
    // MARK: Handler
    private func configureHandler() -> Void{
        self.handler = GMRegisterHandler(viewController: self)
    }
    
    // MARK: View
    private func configureView() {
    }
    
    // MARK: Navigation Bar
    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
        //        self.navigationItem.leftBarButtonItem = UIBarButtonItem.barButton(image: UIImage(named: ApplicationImageFilenames.kNavigationRightBarMenu), frame: CGRect(x: 0, y: 0, width: CGFloat(20), height: CGFloat(20)), target: self, action:#selector(menuButtonTapped))
        //        self.navigationItem.showNavigationBar(text: "HOME")
        //        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    // MARK: Callbacks
    func configureCallBacks(){
        self.registerView.didPressLoginButtonCallback = {
            self.handleLoginButtonTappedEvent()
        }
        
        self.registerView.didPressSignUpButtonCallback = {
            self.handleSignUpButtonTappedEvent()
        }
    }
    
    func handlePushNotifcation(userName:String) {
        
        
        let userReference = Database.database().reference()
        userReference.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            if let result = snapshot.children.allObjects as? [DataSnapshot] {
                for child in result {
                    print(child.key)
                    Database.database().reference().child("users").child(child.key).child("onesignal").observeSingleEvent(of: .value, with: {snapshot in
                        if snapshot.exists() {
                            
                            for (playerId, _) in snapshot.value as! Dictionary<String, String> {
                                
                                OneSignal.postNotification(["headings": ["en": "ewdewee"], "contents": ["en": "One New House Register with name" + userName], "include_player_ids": [playerId]])
                            }
                        }
                    })

                    self.treeUserIds.append(child.key)
                }}})
    }
    
    func fetchReferals() {
        if Auth.auth().currentUser != nil {
            
            let userReference = Database.database().reference()
            self.registerView.hometable.collectionViewData.removeAll()
            self.referalData.removeAll()
            userReference.child("Referrals").child((Auth.auth().currentUser?.uid)!).observe(.childAdded, with: { (snapshot) in
                
                //Convert the info of the data into a string variable
                self.fetchedReferals = snapshot.value as? [String:Any]
                
                let userDataObject = UserClass()
                userDataObject.fetchRefferal(snap: snapshot)
                self.referalData.append(userDataObject)
                self.referalIds.append(snapshot.key)
                self.registerView.hometable.collectionViewData.append(userDataObject)
                self.registerView.hometable.reloadData()
                self.registerView.numberOfReferals.text = String(self.referalData.count)
                self.registerView.noReferalLabel.text = ""
                self.checkDataRequire()
            })
        }
    }
    

    
    private func signUpButtonTapped() {
        
//        stripCard.validate
        
        
        
        let firstName = registerView.nameTextfield.text
        let email = registerView.emailTextfield.text
        let password = registerView.passwordTextfield.text
        let conformPassword = registerView.confromPasswordTextfield.text
        let location = registerView.locationTextfield.text
        let phone = registerView.phoneTextfield.text
        let requirement = registerView.requirementTextfield.text
        
        let latitude = "3223.2343"
        let logtude = "2332432.0"
        
        if firstName != "" && password != "" && conformPassword != "" && requirement != "" &&
            phone != "" && email != "" {
            if location != "" {
            Auth.auth().createUser(withEmail: email!, password: password!) { (user, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
                else if let user = user {
                    print(user)
                    let userReference = Database.database().reference()
                    let userDetail: NSDictionary = ["name": firstName!,
                                                    "phone": phone!,
                                                    "address": location!,
                                                    "latitude": self.userLocationLatitude!,
                                                    "longitude": self.userLocationLongitude!,
                                                    "type": "house",
                                                    "requirements": requirement!
                    ]
                    
                    let childUpdates = ["/HouseUser/\(user.uid)": userDetail]
                    userReference.updateChildValues(childUpdates)
                    (UIApplication.shared.delegate as! AppDelegate).moveToLoginViewController()
                    self.handlePushNotifcation(userName: firstName!)

                }
            }
            }else {
                self.showAlert("Kindly Check Your Internet Or Set Allow location while using App")
            }
        }else {
            self.showAlert("All fields are mendatory")
        }
    }
    
    private func checkDataRequire() {
        self.stopAnimating()
        if referalData.count <= 0 {
            self.registerView.numberOfReferals.text = ""
            self.registerView.noReferalLabel.text = "No Refferal Found"
        }
    }
    
    // MARK: Events
    private func handleLoginButtonTappedEvent() {
        (UIApplication.shared.delegate as! AppDelegate).moveToLoginViewController()
    }
    
    private func handleSignUpButtonTappedEvent() {
        self.signUpButtonTapped()
    }
    
    private func handleTableCellButtonTappedEvent(index:Int) {
        //        (UIApplication.shared.delegate as! AppDelegate).moveToStaticReferalViewController(userDetail: referalData[index], referalIds: referalIds,selectedIndex:index)
    }
    
    func showAlert(_ message: String) {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    @objc func menuButtonTapped() {
        self.handleMenuButtonTappedEvent()
    }
    
    private func handleMenuButtonTappedEvent(){
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    func fetchCountryAndCity(location: CLLocation, completion: @escaping (String, String) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            if let error = error {
                print(error)
            } else if let country = placemarks?.first?.country,
                let city = placemarks?.first?.locality {
                completion(country, city)
            }
        }
    }
    
    
}
