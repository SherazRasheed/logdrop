//
//  GMLoginHandler.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation

class GMLoginHandler: NSObject {
    // MARK: Instance Variables
    var viewController: GMLoginViewController!
    var view: GMLoginView!
    
    // MARK: Init
    required init(viewController: GMLoginViewController!) {
        self.viewController = viewController
        self.view = self.viewController.loginView
    }
    
    //MARK: Callbacks

}

