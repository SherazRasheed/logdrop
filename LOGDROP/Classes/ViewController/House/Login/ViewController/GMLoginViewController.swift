//
//  GMLoginViewController.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit
import Firebase
import PINCache

class GMLoginViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var loginView: GMLoginView!
    //    var fireBaseReference: FIRDatabaseReference?
    //    var dataBaseHandler: FIRDatabaseHandle?
    
    
    // MARK: Instance Variables
    var handler: GMLoginHandler!
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Configurations
        self.configureNavigationBar()
        self.configureHandler()
        self.configureCallBacks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        self.configureView()
    }
    
    // MARK: Private Methods
    
    
    
    //MARK: Gmail Login
    
    // MARK: Navigation Bar
    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: Handler
    private func configureHandler() -> Void{
        self.handler = GMLoginHandler(viewController: self)
    }
    
    // MARK: View
    private func configureView() {
    }
    
    // MARK: Callbacks
    func configureCallBacks(){
        self.loginView.didPressLoginButtonCallback = {
            self.Login()
        }
        
        self.loginView.didPressSignUpButtonCallback = {
            self.handleSignUpButtonTappedEvent()
        }
        
        self.loginView.didPressforgotPasswordButtonCallback = {
            self.presentAlert()
        }
    }
    
    
    func presentAlert() {
        let alertController = UIAlertController(title: "Email", message: "Please Enter your email:", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let field = alertController.textFields?[0] {
                Auth.auth().sendPasswordReset(withEmail: field.text!) { error in
                    
                }
                
            } else {
                // user did not fill field
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Email"
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func Login(){
        if (self.loginView.emailTextField.text?.isEmpty == false && self.loginView.passwordTextField.text?.isEmpty == false){
            
            Auth.auth().signIn(withEmail: (self.loginView.emailTextField.text)!, password: self.loginView.passwordTextField.text!) { (user, error) in
                if error != nil {
                    print(error?.localizedDescription)
                    self.showAlert("Email & password not matched")
                    
                }else if let user = Auth.auth().currentUser {
                    PINCache.shared().setObject("success" as NSCoding, forKey: "Login")
                    (UIApplication.shared.delegate as! AppDelegate).moveToPayementViewController()
                }
            }}
        else {
            showAlert("Email & password not matched")
        }
    }
    
    func checkEmail() -> Bool{
        if (self.loginView.emailTextField.text?.isEmpty == false && self.loginView.passwordTextField.text?.isEmpty == false){
            
            Auth.auth().signIn(withEmail: self.loginView.emailTextField.text!, password: self.loginView.passwordTextField.text!) { (user, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
                else if let user = user {
                    print(user)
                }
            }
            
            PINCache.shared().setObject("success" as NSCoding, forKey: "Login")
            //            (UIApplication.shared.delegate as! AppDelegate).moveToAffiliateViewController()
            
            //            for i in 0..<donorData.count{
            //                if loginView.emailTextField.text! == donorData[i].email{
            //                    print(donorData[i].email)
            //                    if loginView.passwordTextField.text! == donorData[i].password{
            //                        UserDefaults.standard.set(self.loginView.emailTextField.text, forKey: "Email")
            //                        UserDefaults.standard.setValue(donorData[i].name, forKey: "UserName")
            //
            //                        print(donorData[i].email)
            //                        return true
            //                    }
            //                    else{
            //                        showAlert("Enter Correct Password")
            //                        return false
            //
            //                    }
            //                }
            //                else{
            //
            //                    showAlert("Enter Valid Email")
            //                    return false
            //
            //                }
            //            }
            return false
        }
        else{
            showAlert("Email & password not matched")
            return false
        }
    }
    
    // MARK: Events
    private func handleSignUpButtonTappedEvent() {
        (UIApplication.shared.delegate as! AppDelegate).moveToRegisterViewController()
    }
    
    func showAlert(_ message: String) {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    class func isPasswordValidation(password: String , confirmPassword : String) -> Bool {
        if password.characters.count <= 7 && confirmPassword.characters.count <= 7{
            
            if password == confirmPassword{
                return true
            }
            else{
                return false
            }
        }
        else{
            return false
        }
        
    }
}
