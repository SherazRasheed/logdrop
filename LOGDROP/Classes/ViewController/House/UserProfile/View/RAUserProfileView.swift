//
//  GMView.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation
import UIKit
import AAPickerView
import SkyFloatingLabelTextField

class RAUserProfileView: UIView {
    // MARK: Outlets
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var firstNameTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var secondNameTextfield: SkyFloatingLabelTextField!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var cityTextfield: SkyFloatingLabelTextField!
    
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var typeOfIndustoryTextfield: AAPickerView!
    
    @IBOutlet weak var zipCodeLabel: UILabel!
    @IBOutlet weak var zipCodeTextfield: SkyFloatingLabelTextField!
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var phoneNumberTextfield: SkyFloatingLabelTextField!
    
    @IBOutlet weak var commentTextfield: SkyFloatingLabelTextField!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextfield: SkyFloatingLabelTextField!
    
    @IBOutlet weak var buisnessTypeTextfield: SkyFloatingLabelTextField!
    
    @IBOutlet weak var stateTextField: AAPickerView!
    
    @IBOutlet weak var typeOfIndustoryTextField: AAPickerView!

    override func awakeFromNib() {

    }
    
    public var didPressLoginButtonCallback : (() -> Void)?
    public var didPressSignUpButtonCallback : (() -> Void)?

    // MARK: Action Methods
    @IBAction func loginButtonPressed(_ sender: Any) {
        self.handleLoginButtonTapEvent()
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        self.handleSignUpButtonTapEvent()
    }
    
    //MARK: Events
    private func handleLoginButtonTapEvent() {
        if didPressLoginButtonCallback != nil {
            didPressLoginButtonCallback!()
        }
    }
    
    private func handleSignUpButtonTapEvent() {
        if didPressSignUpButtonCallback != nil {
            didPressSignUpButtonCallback!()
        }
    }
}

