//
//  GMRegisterViewController.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit
import Firebase
import KYDrawerController
import PINCache

class RAUserProfileViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var userProfileViewView: RAUserProfileView!
    
    // MARK: Instance Variables
    var handler: RAUserProfileHandler!
    
    var fetchedReferals:[String:String]?
    
    var referalData = [UserClass]()
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Configurations
        self.configureNavigationBar()
        self.configureHandler()
        self.configureCallBacks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchUserProfile()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        self.configureView()
    }
    
    // MARK: Private Methods
    
    // MARK: Navigation Bar
    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.barButton(image: UIImage(named: ApplicationImageFilenames.kNavigationRightBarMenu), frame: CGRect(x: 0, y: 0, width: CGFloat(20), height: CGFloat(20)), target: self, action:#selector(menuButtonTapped))
        self.navigationItem.showNavigationBar(text: "Profile")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func fetchUserProfile() {
        if Auth.auth().currentUser != nil {
            
            let userReference = Database.database().reference()
            
            userReference.child("Affiliates").child((Auth.auth().currentUser?.uid)!).observe(.value, with: { (snapshot) in
                
                
                
                //Convert the info of the data into a string variable
                self.fetchedReferals = snapshot.value as? [String:String]
            })
        }
    }
    
    // MARK: Handler
    private func configureHandler() -> Void{
        self.handler = RAUserProfileHandler(viewController: self)
    }
    
    // MARK: View
    private func configureView() {
    }
    
    // MARK: Callbacks
    func configureCallBacks(){
        self.userProfileViewView.didPressSignUpButtonCallback = {
            (UIApplication.shared.delegate as! AppDelegate).moveToLoginViewController()
    }
        
        self.userProfileViewView.didPressLoginButtonCallback = {
            (UIApplication.shared.delegate as! AppDelegate).moveToTreeLoginViewController()
        }
        
    }
    
    private func signUpButtonTapped() {
        
    }
    
    // MARK: Events
    private func handleLoginButtonTappedEvent() {
    }
    
    private func handleSignUpButtonTappedEvent() {
    }
    
    func showAlert(_ message: String) {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    // MARK: Action Methods
    @objc func menuButtonTapped() {
        self.handleMenuButtonTappedEvent()
    }
    
    private func handleMenuButtonTappedEvent(){
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
}
