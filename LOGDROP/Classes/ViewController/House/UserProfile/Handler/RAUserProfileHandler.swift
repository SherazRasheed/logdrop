//
//  GMRegisterHandler.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation

class RAUserProfileHandler: NSObject {
    // MARK: Instance Variables
    var viewController: RAUserProfileViewController!
    var view: RAUserProfileView!
    
    // MARK: Init
    required init(viewController: RAUserProfileViewController!) {
        self.viewController = viewController
        self.view = self.viewController.userProfileViewView
    }
    
    //MARK: Callbacks
    
}

