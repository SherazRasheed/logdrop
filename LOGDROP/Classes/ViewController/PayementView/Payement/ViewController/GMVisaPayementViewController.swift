//
//  GMLoginViewController.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit
import Firebase
import PINCache
import Stripe
import AFNetworking

class GMVisaPayementViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var visaPayementView: GMVisaPayementView!
    
    // MARK: Instance Variables
    var handler: GMVisaPaymentHandler!
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Configurations
        self.configureNavigationBar()
        self.configureHandler()
        self.configureCallBacks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        self.configureView()
    }
    
    // MARK: Private Methods
    //MARK: Gmail Login
    
    // MARK: Navigation Bar
    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: Handler
    private func configureHandler() -> Void{
        self.handler = GMVisaPaymentHandler(viewController: self)
    }
    
    // MARK: View
    private func configureView() {
    }
    
    // MARK: Callbacks
    func configureCallBacks(){
        self.visaPayementView.didPressSignUpButtonCallback = {
            self.submitCardInformation()
        }
        
        self.visaPayementView.didPressLoginButtonCallback = {
            (UIApplication.shared.delegate as! AppDelegate).moveToPayementViewController()
        }
    }
    
    // MARK: Events
    private func handleSignUpButtonTappedEvent() {
        (UIApplication.shared.delegate as! AppDelegate).moveToRegisterViewController()
    }
    
    func showAlert(_ message: String) {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func submitCardInformation() {
        
        let cardNumber = visaPayementView.cardNumberTextField.text
        let cvc = visaPayementView.cvcTextField.text
        let expireyMonth = visaPayementView.expireyMonthTextField.text
        let expireyYear = visaPayementView.expireyYearTextField.text
        
        if cardNumber != "" && cvc != "" && expireyMonth != "" && expireyYear != "" {
            
            
            var stripCard = STPCard()
            
            // Split the expiration date to extract Month & Year
            let expirationDate = "09/2019"
            let expMonth = "09"
            let expYear = "2019"
            
            // Send the card info to Strip to get the token
            stripCard.number = cardNumber
            stripCard.cvc = cvc
            stripCard.expMonth = UInt(expireyMonth!)!
            stripCard.expYear = UInt(expireyYear!)!
            
            var underlyingError: NSError?
            //    stripCard.validateCardReturningError(&underlyingError)
            //    stripCard.validateValue(, forKey: )
            if underlyingError != nil {
                //       self.spinner.stopAnimating()
                self.handleError(error: underlyingError!)
                return
            }
            
            STPAPIClient.shared().createToken(withCard: stripCard, completion: { (token, error) -> Void in
                
                if error != nil {
                    self.handleError(error: error! as NSError)
                    return
                }
                
                self.postStripeToken(token: token!)
            })
        }
    }
    
    func handleError(error: NSError) {
        UIAlertView(title: "Please Try Again",
                    message: error.localizedDescription,
                    delegate: nil,
                    cancelButtonTitle: "OK").show()
        
    }
    
    func postStripeToken(token: STPToken) {
        
        let URL = "http://localhost/donate/payment.php"
        let params = ["stripeToken": token.tokenId,
                      "amount": 10,
                      "currency": "usd",
                      "description": "sheraz.iap@gmail.com"] as [String : Any]
        
        let manager = AFHTTPRequestOperationManager()
        manager.post(URL, parameters: params, success: { (operation, responseObject) -> Void in
            
            if let response = responseObject as? [String: String] {
                UIAlertView(title: response["status"],
                            message: response["message"],
                            delegate: nil,
                            cancelButtonTitle: "OK").show()
                (UIApplication.shared.delegate as! AppDelegate).moveToMapViewController()
            }
            
        }) { (operation, error) -> Void in
            self.handleError(error: error! as NSError)
        }
    }
}
