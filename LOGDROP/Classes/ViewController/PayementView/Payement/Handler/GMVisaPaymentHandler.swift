//
//  GMLoginHandler.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation

class GMVisaPaymentHandler: NSObject {
    // MARK: Instance Variables
    var viewController: GMVisaPayementViewController!
    var view: GMVisaPayementView!
    
    // MARK: Init
    required init(viewController: GMVisaPayementViewController!) {
        self.viewController = viewController
        self.view = self.viewController.visaPayementView
    }
    
    //MARK: Callbacks

}

