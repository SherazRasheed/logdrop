//
//  GMLoginView.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation
import UIKit

class GMVisaPayementView: UIView {
    // MARK: Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
   
    @IBOutlet weak var signInText: UILabel!
    @IBOutlet weak var cvcTextField: UITextField!

    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var expireyMonthTextField: UITextField!
    @IBOutlet weak var expireyYearTextField: UITextField!

    
    
    //MARK: Callbacks
    public var didPressSignUpButtonCallback : (() -> Void)?
    public var didPressLoginButtonCallback : (() -> Void)?

    public var didPressfacebookButtonCallback : (() -> Void)?

    public var didPressGmailButtonCallback : (() -> Void)?

    public var didPressforgotPasswordButtonCallback : (() -> Void)?

    override func awakeFromNib() {
    
    }
    
    // MARK: Action Methods
    @IBAction func facebookButtonPressed(_ sender: Any) {
        self.handlefacebookButtonTapEvent()
    }
    
    @IBAction func gmailButtonPressed(_ sender: Any) {
        self.handleGmailButtonTapEvent()

    }
    
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        self.handleforgotPasswordButtonTapEvent()
    }

    @IBAction func signUpButtonPressed(_ sender: Any) {
        self.handleSignUpButtonTapEvent()
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        self.handleLoginButtonTapEvent()
    }
    //MARK: Events
    private func handleLoginButtonTapEvent() {
        if didPressLoginButtonCallback != nil {
            didPressLoginButtonCallback!()
        }
    }
    
    private func handleSignUpButtonTapEvent() {
        if didPressSignUpButtonCallback != nil {
            didPressSignUpButtonCallback!()
        }
    }
    
    private func handlefacebookButtonTapEvent() {
        if didPressfacebookButtonCallback != nil {
            didPressfacebookButtonCallback!()
        }
    }
    
    private func handleGmailButtonTapEvent() {
        if didPressGmailButtonCallback != nil {
            didPressGmailButtonCallback!()
        }
    }
    
    private func handleforgotPasswordButtonTapEvent() {
        if didPressforgotPasswordButtonCallback != nil {
            didPressforgotPasswordButtonCallback!()
        }
    }
}
