//
//  GMRegisterViewController.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit
import Firebase
import PINCache
import KYDrawerController
import NVActivityIndicatorView
import CoreLocation
import OneSignal

class GMTreeRegisterViewController: UIViewController,CLLocationManagerDelegate {
    // MARK: Outlets
    @IBOutlet weak var treeRegisterView: GMTreeRegisterView!
    var locationManager:CLLocationManager!
    
    // MARK: Instance Variables
    var handler: GMTreeRegisterHandler!
    var referalIds = [String]()
    var fetchedReferals:[String:Any]?
    var referalData = [UserClass]()
    var userLocationLatitude:String?
    var userLocationLongitude: String?
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations[0]
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        
        manager.stopUpdatingLocation()
        
        userLocationLatitude = String(userLocation.coordinate.latitude)
        userLocationLongitude = String(userLocation.coordinate.longitude)
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        fetchCountryAndCity(location: userLocation) { country, city in
            print("country:", country)
            print("city:", city)
            self.treeRegisterView.locationTextfield.text = city + " " + country
        }
        
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Configurations
        //        self.configureIndicator()
        self.configureNavigationBar()
        self.configureHandler()
        self.configureCallBacks()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.determineMyCurrentLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        self.configureView()
    }
    
    // MARK: Private Methods
    
    // MARK: Navigation Bar
    
    
    // MARK: Handler
    private func configureHandler() -> Void{
        self.handler = GMTreeRegisterHandler(viewController: self)
    }
    
    // MARK: View
    private func configureView() {
    }
    
    // MARK: Navigation Bar
    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: Callbacks
    func configureCallBacks(){
        self.treeRegisterView.didPressLoginButtonCallback = {
            self.handleLoginButtonTappedEvent()
        }
        
        self.treeRegisterView.didPressSignUpButtonCallback = {
            self.handleSignUpButtonTappedEvent()
        }
    }
    
    private func signUpButtonTapped() {
        let firstName = treeRegisterView.nameTextfield.text
        let email = treeRegisterView.emailTextfield.text
        let password = treeRegisterView.passwordTextfield.text
        let conformPassword = treeRegisterView.confromPasswordTextfield.text
        let location = treeRegisterView.locationTextfield.text
        let phone = treeRegisterView.phoneTextfield.text
        
        if firstName != "" && location != "" && password != "" && conformPassword != "" &&
            phone != "" && email != "" {
            Auth.auth().createUser(withEmail: email!, password: password!) { (user, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
                else if let user = user {
                    print(user)
                    let userReference = Database.database().reference()
                    let userDetail: NSDictionary = ["name": firstName!,
                                                    "phone": phone!,
                                                    "address": location!,
                                                    "latitude": self.userLocationLatitude!,
                                                    "longitude": self.userLocationLongitude!,
                                                    "type": "tree"
                    ]
                    
                    let childUpdates = ["/TreeUser/\(user.uid)": userDetail]
                    userReference.updateChildValues(childUpdates)
                    (UIApplication.shared.delegate as! AppDelegate).moveToTreeLoginViewController()
                    
                    OneSignal.registerForPushNotifications()
                    OneSignal.idsAvailable({(_ userId, _ pushToken) in
                        print("UserId:\(userId!)")
                        if pushToken != nil {
                            print("pushToken:\(pushToken!)")
                            Database.database().reference().child("users").child(user.uid).child("onesignal").setValue([userId!: pushToken!])
                            
                        }
                    })

                }
            }
        }else {
            self.showAlert("All fields are mendatory")
        }
    }
    
    private func checkDataRequire() {
        self.stopAnimating()
        if referalData.count <= 0 {
            self.treeRegisterView.numberOfReferals.text = ""
            self.treeRegisterView.noReferalLabel.text = "No Refferal Found"
        }
    }
    
    // MARK: Events
    private func handleLoginButtonTappedEvent() {
        (UIApplication.shared.delegate as! AppDelegate).moveToLoginViewController()
    }
    
    private func handleSignUpButtonTappedEvent() {
        self.signUpButtonTapped()
    }
    
    private func handleTableCellButtonTappedEvent(index:Int) {
        //        (UIApplication.shared.delegate as! AppDelegate).moveToStaticReferalViewController(userDetail: referalData[index], referalIds: referalIds,selectedIndex:index)
    }
    
    func showAlert(_ message: String) {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    @objc func menuButtonTapped() {
        self.handleMenuButtonTappedEvent()
    }
    
    private func handleMenuButtonTappedEvent(){
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    func determineMyCurrentLocation() {
        
        
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
        }
    }
    
    func fetchCountryAndCity(location: CLLocation, completion: @escaping (String, String) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            if let error = error {
                print(error)
            } else if let country = placemarks?.first?.country,
                let city = placemarks?.first?.locality {
                completion(country, city)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
}

