//
//  GMRegisterHandler.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation

class GMTreeRegisterHandler: NSObject {
    // MARK: Instance Variables
    var viewController: GMTreeRegisterViewController!
    var view: GMTreeRegisterView!
    
    // MARK: Init
    required init(viewController: GMTreeRegisterViewController!) {
        self.viewController = viewController
        self.view = self.viewController.treeRegisterView
    }
    
    //MARK: Callbacks
    
}

