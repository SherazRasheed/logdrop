//
//  GMLoginHandler.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation

class GMTreeLoginHandler: NSObject {
    // MARK: Instance Variables
    var viewController: GMTreeLoginViewController!
    var view: GMTreeLoginView!
    
    // MARK: Init
    required init(viewController: GMTreeLoginViewController!) {
        self.viewController = viewController
        self.view = self.viewController.treeLoginView
    }
    
    //MARK: Callbacks

}

