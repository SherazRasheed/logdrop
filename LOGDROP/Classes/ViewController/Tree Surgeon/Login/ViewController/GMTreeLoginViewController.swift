//
//  GMLoginViewController.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit
import Firebase
import PINCache

class GMTreeLoginViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var treeLoginView: GMTreeLoginView!
    
    // MARK: Instance Variables
    var handler: GMTreeLoginHandler!
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Configurations
        self.configureNavigationBar()
        self.configureHandler()
        self.configureCallBacks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        self.configureView()
    }
    
    // MARK: Private Methods
    //MARK: Gmail Login
    
    // MARK: Navigation Bar
    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: Handler
    private func configureHandler() -> Void{
        self.handler = GMTreeLoginHandler(viewController: self)
    }
    
    // MARK: View
    private func configureView() {
    }
    
    // MARK: Callbacks
    func configureCallBacks(){
        self.treeLoginView.didPressLoginButtonCallback = {
            self.Login()
        }
        
        self.treeLoginView.didPressSignUpButtonCallback = {
            self.handleSignUpButtonTappedEvent()
        }
        
        self.treeLoginView.didPressforgotPasswordButtonCallback = {
            self.presentAlert()
        }
    }
    
    
    func presentAlert() {
        let alertController = UIAlertController(title: "Email", message: "Please Enter your email:", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let field = alertController.textFields?[0] {
                Auth.auth().sendPasswordReset(withEmail: field.text!) { error in
                    
                }
                
            } else {
                // user did not fill field
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Email"
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func Login(){
        if (self.treeLoginView.emailTextField.text?.isEmpty == false && self.treeLoginView.passwordTextField.text?.isEmpty == false){
            
            Auth.auth().signIn(withEmail: (self.treeLoginView.emailTextField.text)!, password: self.treeLoginView.passwordTextField.text!) { (user, error) in
                if error != nil {
                    print(error?.localizedDescription)
                    self.showAlert("Email & password not matched")
                    
                }else if let user = Auth.auth().currentUser {
                    PINCache.shared().setObject("success" as NSCoding, forKey: "Login")
                    (UIApplication.shared.delegate as! AppDelegate).moveToPayementViewController()
                    
                }
            }}
        else {
            showAlert("Email & password not matched")
        }
    }
    
    func checkEmail() -> Bool{
        
        
        if (self.treeLoginView.emailTextField.text?.isEmpty == false && self.treeLoginView.passwordTextField.text?.isEmpty == false){
            
            Auth.auth().signIn(withEmail: self.treeLoginView.emailTextField.text!, password: self.treeLoginView.passwordTextField.text!) { (user, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
                else if let user = user {
                    print(user)
                }
            }
            
            PINCache.shared().setObject("success" as NSCoding, forKey: "Login")
            
            return false
        }
        else{
            showAlert("Email & password not matched")
            return false
        }
    }
    
    // MARK: Events
    private func handleSignUpButtonTappedEvent() {
        (UIApplication.shared.delegate as! AppDelegate).moveToTreeRegisterViewController()
    }
    
    func showAlert(_ message: String) {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    class func isPasswordValidation(password: String , confirmPassword : String) -> Bool {
        if password.characters.count <= 7 && confirmPassword.characters.count <= 7{
            
            if password == confirmPassword{
                return true
            }
            else{
                return false
            }
        }
        else{
            return false
        }
        
    }
}
