//
//  GMLoginHandler.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation

class GMPayementHandler: NSObject {
    // MARK: Instance Variables
    var viewController: GMPayementViewController!
    var view: GMPayementView!
    
    // MARK: Init
    required init(viewController: GMPayementViewController!) {
        self.viewController = viewController
        self.view = self.viewController.loginView
    }
    
    //MARK: Callbacks

}

