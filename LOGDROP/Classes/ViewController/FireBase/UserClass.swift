//
//  UserClass.swift
//  LOGDROP
//
//  Created by Shery on 18/08/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation
import Firebase


class DataClass {
    
    var firstName = String()
    var email = String()
    var secondName = String()
    var phone = String()
    var state = String()
    var zip = String()
    var address = String()
    var city = String()
    var comments = String()
    var is_known_to_him = Bool()
    var kind = String()
    var id = String()
    var userReference: DatabaseReference!
    
    func fetchRefferal(snap: DataSnapshot) {
        
        let userDict = snap.value as! [String: Any]
        
        self.id = snap.key
        
        self.firstName = userDict["name"] as! String
        self.secondName = userDict["last_name"] as! String
        self.email = userDict["email"] as! String
        self.phone = userDict["phone"] as! String
        self.state = userDict["state"] as! String
        self.zip = userDict["zip"] as! String
        self.address = userDict["address"] as! String
        self.city = userDict["city"] as! String
        self.comments = userDict["comments"] as! String
        self.is_known_to_him = userDict["is_known_to_him"] as! Bool
        self.kind = userDict["kind"] as! String
    }
    
    func fetchUser(snap: DataSnapshot) {
        
        let userDict = snap.value as! [String: Any]
        
        self.id = snap.key
        
        self.firstName = userDict["name"] as! String
        self.secondName = userDict["last_name"] as! String
        self.email = userDict["email"] as! String
        self.phone = userDict["phone"] as! String
        self.state = userDict["state"] as! String
        self.zip = userDict["zip"] as! String
        self.address = userDict["address"] as! String
        self.city = userDict["city"] as! String
    }
    
    func addNewUser(name:String,email:String,phoneNumber:String,location:String,bloodGroup:String,availble:String){
        
        userReference = Database.database().reference()
        
        let key = userReference.child("Users").childByAutoId().key
        
        let user: NSDictionary = ["name": firstName,
                                  "email": email,
                                  "phone": phone,
                                  "last_name": secondName,
                                  "state": state,
                                  "zip": zip,
                                  "address": address,
                                  "city": city,
                                  "comments": comments,
                                  "is_known_to_him": is_known_to_him,
                                  "kind": kind
        ]
        let childUpdates = ["/posts/\(key)": user]
        userReference.updateChildValues(childUpdates)
    }
}
