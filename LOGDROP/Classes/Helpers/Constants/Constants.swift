
//  ApplicationConstants.swift
//  Expo Mobile
//
//  Created by Onebyte LLC on 6/9/17.
//  Copyright © 2017 Onebyte LLC. All rights reserved.
//

import Foundation

struct ApplicationStringConstants {
    // MARK: Navigation Bar
    static let kNavigationTitle = "Expo Mobile"
    static let kSearchTitle = "Search"
    static let kUserName = "Jane Doe"
    static let kUserMobileNumber = "(208)-555-5555"
    static let kUsageType = "Usage Type"
    static let kStartDate = "Start Date"
    static let kEndDate = "End Date"
    static let kcallNumber = "00000000000"
    static let kSelectedUsageType = "Select One"
    static let kUsageTypeTitle = "Issue Category"
    static let kBillingTitle = "Billing"
    static let kServiceTitle = "Service"
    static let kDeviceTitle  = "Device"
    static let kPortingTitle = "Porting"
    static let kNoDataTitle = "N/A"
    static let kDateFormate = "yyyy-MM-dd"
    static let kSmsTitle = ",WSMS"
    static let kVoiceTitle = "W611"
}

struct EMPayementHistoryTableViewConstants {
    // TODO: Put 'f' against floats
    
    static let kNumberOfSections = 1
    static let kInitialNumberOfRows = 25
    static let kSecondNumberOfRows = 0
    static let kHeightForHeaderRatio = 80.0
    static let kHeightForItemRow = 100.0
    static let kHeightForFooter = 0.0
}

struct EMSideMenuTableViewConstants {
    static let kNumberOfSections = 2
    static let kInitialNumberOfRows = 10
    static let kSecondNumberOfRows = 0
    static let kHeightForHeaderRatio = 190.0
    static let kHeightForTypeHeaderRow = 50.0
    static let kHeightForItemRow = 100.0
    static let kHeightForFooter = 0.0
    
    static let kTopSectionIndex = 0
    static let kHomeSectionIndex = 1
    static let kOrderLogsSectionIndex = 2
    
    static let kCurrentOrdersIndex = 0
}

struct ApplicationNumberConstants {
    // MARK: Navigation Bar
    static let kMenuBarButtonWidth = 30.0
    static let kMenuBarButtonHeight = 70.0
    
    static let kLeftBarButtonWidth = 30.0
    static let kLeftBarButtonHeight = 30.0
    
    static let kNavigationTitleImageWidth = 61.6
    static let kNavigationTitleImageHeight = 55.0
    
    // MARK: Side Menu
    static let kMenuRightPadding = 60
    static let kMenuRowCornerRadius = 5.0
    static let kMenuBackgroundAlphaComponent = 0.3
    static let kMenuBottomPadding = 60.0
    
    // MARK:UIControls
    static let kControlsBorderWidth = 0.5
    
    //MARK: Textfields
    static let kTextfieldCornerRadius = 10.0
    static let kTextfieldBorderWidth = 1.0
    static let kTextfieldLeftPadding = 15.0
    
    static let kdrawyerWidth = 100.0

}

struct ApplicationColorCodes {
    static let kNavigationColorCode = "1EB6B5"
}

struct ApplicationLocalArrays {
    // MARK: Side Menu
   
  static let kStatesList = [ "Alabama- AL",
    "Alaska - AK",
    "Arizona - AZ",
    "Arkansas - AR",
    "California - CA",
    "Colorado - CO",
    "Connecticut - CT",
    "Delaware - DE",
    "Florida - FL",
    "Georgia - GA",
    "Hawaii - HI",
    "Idaho - ID",
    "Illinois - IL",
    "Indiana - IN",
    "Iowa - IA",
    "Kansas - KS",
    "Kentucky - KY",
    "Louisiana - LA",
    "Maine - ME",
    "Maryland - MD",
    "Massachusetts - MA",
    "Michigan - MI",
    "Minnesota - MN",
    "Mississippi - MS",
    "Missouri - MO",
    "Montana - MT",
    "Nebraska - NE",
    "Nevada - NV",
    "New Hampshire - NH",
    "New Jersey - NJ",
    "New Mexico - NM",
    "New York - NY",
    "North Carolina - NC",
    "North Dakota - ND",
    "Ohio - OH",
    "Oklahoma - OK",
    "Oregon - OR",
    "Pennsylvania - PA",
    "Rhode Island - RI",
    "South Carolina - SC",
    "South Dakota - SD",
    "Tennessee - TN",
    "Texas - TX",
    "Utah - UT",
    "Vermont - VT",
    "Virginia - VA",
    "Washington - WA",
    "West Virginia - WV",
    "Wisconsin - WI",
    "Wyoming - WY"]
    
    static let kTypeOfIndustory = ["AUTO","MEDICAL","RESTAURANT","RETAIL","SERVICES","WEB"]

    static let kLoginMainHome = ["HOME","USER PROFILE","REFER A FUTURE AFFILIATE","REFER A POTENTIAL CUSTOMER","LOGOUT"]
    static let kLogoutMainHome = ["HOME","My ACCOUNT","MATCHES", "LEADERBOARD","HOW TO PLAY","LOGOUT"]
    
    static let kGameName = [" Infinite Warfare(Xbox One/PS4)","NBA 2K17(Xbox One/PS4)","Madden 17(Xbox One/PS4)", "FIFA 17(Xbox One/PS4)","NHL 2K17(Xbox One/PS4)","Overwatch(Xbox One/PS4)","Destiny(Xbox One/PS4)","Gears of War 4(Xbox One)","Halo 5(Xbox One)","Mortal Combat X(Xbox One/PS4)"]

    static let kMenuOptions = ["home", "Refill", "Usage", "Logout"]
    static let kChangePlanOptions = ["PRICE","DATA", "TEXT", "VOICE"]
    static let kEditAccountOptions = ["PAYMENT", "EMAIL", "ADDRESS", "RESET PASSWORD"]
    
    static let kUsageTypeOptions = [
        ("1", "Data Only")
        , ("2", "Text Only")
        , ("3", "Voice Only")
        , ("4", "View All")
    ]
    
    static let kDummyPhoneNumbersOptions = [
        ("1", "(205) 456 9889")
        , ("2", "(205) 456 9819")
        , ("3", "(206) 456 9889")
        , ("4", "(205) 456 9883")
    ]
    
    static let kMonthOptions = [
        ("1", "January")
        , ("2", "February")
        , ("3", "March")
        , ("4", "April")
        , ("5", "May")
        , ("6", "June")
        , ("7", "July")
        , ("8", "August")
        , ("9", "September")
        , ("10", "October")
        , ("11", "November")
        , ("12", "December")
    ]
    
    static let kYearOptions = [
        ("1", "2017")
        , ("2", "2018")
        , ("3", "2019")
        , ("4", "2020")
        , ("5", "2021")
        , ("6", "2022")
        , ("7", "2023")
        , ("8", "2024")
        , ("9", "2025")
        , ("10", "2026")
        , ("11", "2027")
        , ("12", "2028")
        , ("13", "2029")
        , ("14", "2030")
        
    ]
    
    
    
    static let kMenuOptionsImages = ["icon_home","icon_profile", "refill", "usage", "history", "support", "logout"]
    static let kIssueCategoryOptions = ["Billing","Service", "Device", "Porting"]
    static let kIssueSubCategoryOptions = [0: ["Auto Pay", "Refund", "Make Payement"],
                                           1: ["Major Impact", "Medium Impact","Minor Impact"],
                                           2: ["Programming","Re-provision"],
                                           3: ["Edit","Submit","Check status"]
    ]
    
    static let kBillingOptions = [("", "Auto Pay")
        ,("", "Refund"),
         ("", "Make Payement")
    ]
    
    static let kServiceOptions = [
        ("", "Major Impact")
        , ("", "Medium Impact")
        , ("", "Monor Impact")
    ]
    static let kDeviceOptions = [
        ("", "Programming")
        , ("", "Re-Provision")
    ]
    static let kPortingOptions = [
        ("","Edit")
        ,("","Submit")
        ,("","Check Status")
    ]
    
    static let kNumberOfDays = 1 ... 30
    
}

struct ApplicationKeyStrings {
    // MARK: Caching
    static let kLoggedInCacheKey = "logged-in-cache-key"
    static let kDashboardCacheKey = "dashboard-cache-key"
    static let kMDNCacheKey = "mdn-number"
}

struct ApplicationImageFilenames {
    // MARK: Navigation Bar
    static let kNavigationRightBarMenu = "menu-options"
    
    static let kNavigationCancle = "cancel"

    static let kNavigationTitleImage = "navigation-logo"
    static let kNavigationLeftBarProfile = "user-icon"
    static let kNavigationBackButton = "back-icon"
    
    // MARK: Cells
    static let kMinusSelectedIcon = "minus-red-icon"
    static let kAddUnselectedIcon = "add-icon"
}

struct ApplicationDatasourceFilenames {
    // MARK: Offline JSON
}

struct ApplicationFonts {
    
    static let kNavigationFonts = "Calibri-Bold"
    static let kNavigationFontsSize = 12
    static let kButtonFontsSize = 17
    static let kDodgeFonts = "DODGE"
    // static let kDodgeFonts = "DODGE"
    static let kAmerican_TypeWriter = "American Typewriter"
    
}

struct ApplicationInterfaceFilenames {
    static let kLoginViewController = "GMLoginView"
    
    static let kRegisterViewController = "GMRegisterView"
    
    static let kAffiliateViewController = "RAAffiliateView"
    
    static let kRefferalViewController = "RARefferalView"
    
    static let kPotentailCustomerViewController = "RAPotentialCustomerView"
    
    static let kRecentContestViewController = "GMRecentContestView"

    static let kProfileViewController = "RAUserProfileView"
    static let kContestTableViewCell = "GMContestTableViewCell"
    static let kLeaderBoardTableViewCell = "GMLeaderBoardTableViewCell"
    static let kRecentContestTableViewCell = "GMRecentContestTableViewCell"
    static let kHomeTableViewCell = "GMHomeTableViewCell"
    static let kEditPasswordTableViewCell = "EMEditPasswordTableViewCell"
    
    static let kCongratulationViewController = "GMCongratulationView"
    static let kUsageCustomCellTableViewCell = "EMUsageCustomTableViewCell"
    static let kUsageTopScetionTableViewCell = "EMUsageTopSectionTableViewCell"
    static let kUsageCustomHeaderTableViewCell = "EMUsageCustomHeaderTableViewCell"
    
    
    static let kAddGameViewController = "GMAddGameView"
    static let kPayementHistoryHeaderTableViewCell = "EMPayementHistoryHeaderTableViewCell"
    static let kPayementHistoryTableViewCell = "EMPayementHistoryTableViewCell"
    
    static let kHomeViewController = "GMRegisterView"
    
    static let kSearchViewController = "SearchViewController"
    static let kSearchTableViewCell = "SearchTableViewCell"
    static let kChangePlanRowTableViewCell = "EMChangePlanRowTableViewCell"
    
    static let kChangePlanCategoriesRowTableViewCell = "EMChangePlanCategoriesTableViewCell"
    
    static let kMessagesViewController = "EMMessagesView"
    static let kMessagesHeaderTableViewCell = "EMMessagesHeaderTableViewCell"
    static let kMessagesRowTableViewCell = "EMMessagesRowTableViewCell"
    
    static let kAddMoreViewController = "EMAddMoreView"
    static let kAddMoreHeaderTableViewCell = "EMAddMoreHeaderTableViewCell"
    static let kAddMoreRowTableViewCell = "EMAddMoreRowTableViewCell"
    
    static let kCalenderViewController = "CalendarPopUp"
    
    static let kReportAnIssueViewController = "EMReportAnIssueView"
    
    static let kUpgradePlanViewController = "EMUpgradePlanView"
    
    static let kChangePlanCategoriesViewController = "EMChangePlanCategoriesView"
    
    static let kSideMenuViewController = "SideMenuViewController"
    static let kSideMenuItemTableViewCell = "SideMenuTableViewCell"
}

struct ApplicationAlertMessages {
    // MARK: Common
    static let kAlertTitle = "Alert"
    static let kAlertCancelTitle = "Cancel"
    static let kAlertDoneTitle = "Done"
    static let kAlertYesTitle = "Yes"
    static let kAlertMessageTitle = "Are You Sure"
    static let kAlertDateMessageTitle = "Please enter correct date"
    static let kAlertSelectUsageTypeTitle = "Select UsageType"
    static let kAlertSelectCorrectDateTitle = "Please enter correct date"
}

