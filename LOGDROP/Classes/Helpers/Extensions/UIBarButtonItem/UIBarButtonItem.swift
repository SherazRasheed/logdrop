//
//  UIBarButtonItem.swift
//  GlobalGame
//
//  Created by Shery on 31/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation
import UIKit

extension UIBarButtonItem {
    class func barButton(image: UIImage?, frame: CGRect, target: AnyObject, action: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.frame = frame
        button.addTarget(target, action: action, for: .touchUpInside)
        
        let barButtonItem = UIBarButtonItem(customView: button)
        return barButtonItem
    }
}

extension UINavigationItem {
    
    func showNavigationBar(text:String) {
        let label = UILabel(frame: CGRect(x:10.0, y:0.0, width:50.0, height:40.0))
//        label.font = UIFont(name: ApplicationFonts.kNavigationFonts, size: 20.0)
        label.numberOfLines = 2
        label.text = text
        label.textColor = UIColor.white
        
        label.sizeToFit()
        label.textAlignment = NSTextAlignment.center
        self.titleView = label
        self.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
}
