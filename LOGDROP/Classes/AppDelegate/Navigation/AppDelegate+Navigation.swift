//
//  AppDelegate+Navigation.swift
//  GlobalGame
//
//  Created by Shery on 30/07/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import Foundation
import UIKit
import KYDrawerController
import PINCache

extension AppDelegate {
    // MARK: Init
    func initWindow() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        self.window?.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()
    }
    
    // MARK: Private methods
    // MARK: Navigation Bar Appearance
    // MARK: Navigation Bar Appearance
    private func initializeAppNavigationBar(viewController: UIViewController) -> UINavigationController{
        let navigationColor = UIColor.init(red: 30/255, green: 43/255, blue: 102/255, alpha: 1)
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.barTintColor = navigationColor
        navigationController.navigationBar.isTranslucent = false
        return navigationController
    }
    
    private func initializeRefferalAppNavigationBar(viewController: UIViewController) -> UINavigationController{
        let navigationColor = UIColor.init(red: 172/255, green: 31/255, blue: 45/255, alpha: 1)

        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.barTintColor = navigationColor
        navigationController.navigationBar.isTranslucent = false
        return navigationController
    }
    
    private func initializeSideMenuNavigationBar(viewController: UIViewController) -> UINavigationController{
        let navigationColor = UIColor.white
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.barTintColor = navigationColor
        navigationController.navigationBar.isTranslucent = false
        return navigationController
    }

    private func initializeAppNavigationBarAppearance(viewController: UIViewController) -> UINavigationController{
        
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.barTintColor = UIColor.white
        navigationController.navigationBar.isTranslucent = false
        
        return navigationController
    }
//
//    
//    // MARK: Public methods
//    // MARK: Login Navigation Stack
//    
    public func moveToLoginViewController() {
        let loginViewController = GMLoginViewController(nibName: ApplicationInterfaceFilenames.kLoginViewController, bundle: nil)
        
        self.window?.rootViewController = self.initializeAppNavigationBarAppearance(viewController: loginViewController)
    }
    
    public func moveToTreeLoginViewController() {
        let loginViewController = GMTreeLoginViewController(nibName: "GMTreeLoginView", bundle: nil)
        
        self.window?.rootViewController = self.initializeAppNavigationBarAppearance(viewController: loginViewController)
    }
//
    public func moveToPayementViewController() {
        let loginViewController = GMPayementViewController(nibName: "GMPayementView", bundle: nil)
        
        self.window?.rootViewController = self.initializeAppNavigationBarAppearance(viewController: loginViewController)
    }
    
    public func moveToVisaPayementViewController() {
        let loginViewController = GMVisaPayementViewController(nibName: "GMVisaPayementView", bundle: nil)
        
        self.window?.rootViewController = self.initializeAppNavigationBarAppearance(viewController: loginViewController)
    }
    
    public func moveToMapViewController() {
        let loginViewController = GMMapViewController(nibName: "GMMapView", bundle: nil)
        
        self.window?.rootViewController = self.initializeAppNavigationBarAppearance(viewController: loginViewController)
    }

    // MARK: Register Navigation Stack
    
    public func moveToRegisterViewController() {
        let loginViewController = GMRegisterViewController(nibName: ApplicationInterfaceFilenames.kRegisterViewController, bundle: nil)
        self.window?.rootViewController = self.initializeAppNavigationBarAppearance(viewController: loginViewController)
    }
    
    public func moveToTreeRegisterViewController() {
        let loginViewController = GMTreeRegisterViewController(nibName: "GMTreeRegisterView", bundle: nil)
        self.window?.rootViewController = self.initializeAppNavigationBarAppearance(viewController: loginViewController)
    }
    
    public func moveToProfileViewController() {
        let loginViewController = RAUserProfileViewController(nibName: ApplicationInterfaceFilenames.kProfileViewController, bundle: nil)
        self.window?.rootViewController = self.initializeAppNavigationBarAppearance(viewController: loginViewController)
    }
    
    public func configureUserSession() {
        if ((PINCache.shared().object(forKey: "Login")) != " " as NSCoding as! _OptionalNilComparisonType) {
//            self.moveToHomeViewController()
            print(PINCache.shared().object(forKey: "Login"))
        }else {
            self.moveToLoginViewController()
        }
    }
}
