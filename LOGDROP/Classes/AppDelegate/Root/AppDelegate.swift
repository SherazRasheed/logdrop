//
//  AppDelegate.swift
//  LOGDROP
//
//  Created by Shery on 09/08/2017.
//  Copyright © 2017 CodingCampus. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase
import IQKeyboardManagerSwift
import FirebaseMessaging
import OneSignal
import Stripe
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    static let sharedInstance = AppDelegate()
    var window: UIWindow?
    let googleMapsApiKey = "AIzaSyDjJIGy1-xuM5BZMrojr3Nqn44DzwqcSlI"


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let notifcationType: UIUserNotificationType = [UIUserNotificationType.alert,]
        
        let noticattionSetting = UIUserNotificationSettings(types: notifcationType, categories: nil)
        
        application.registerForRemoteNotifications()
        
        application.registerUserNotificationSettings(noticattionSetting)
        
        GMSServices.provideAPIKey(googleMapsApiKey)
        
        PayPalMobile .initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "YOUR_CLIENT_ID_FOR_PRODUCTION",
                                                                PayPalEnvironmentSandbox: "YOUR_CLIENT_ID_FOR_SANDBOX"])
        
        STPPaymentConfiguration.shared().appleMerchantIdentifier = "your apple merchant identifier"

        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        Stripe.setDefaultPublishableKey("pk_test_NEDyoztX4Zysl35jnbVdgU8f")
        
        OneSignal.initWithLaunchOptions(launchOptions, appId: "eff5d113-bf26-4d1f-9345-3eb705ff58e7", handleNotificationReceived: { (notification) in
            
        }, handleNotificationAction: { (result) in
            let payload: OSNotificationPayload? = result?.notification.payload
            print(payload ?? "")
            
        }, settings: [kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInFocusDisplayOption: OSNotificationDisplayType.notification.rawValue])

                
        IQKeyboardManager.sharedManager().enable = true
        self.moveToProfileViewController()
        FirebaseApp.configure()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    }

}

